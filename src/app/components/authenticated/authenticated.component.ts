import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'authenticated',
  templateUrl: 'authenticated.component.html'
})

export class AuthenticatedComponent implements OnInit {
  constructor() { }

  ngOnInit() { }
}