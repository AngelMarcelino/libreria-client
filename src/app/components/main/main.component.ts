import { Component, OnInit } from '@angular/core';
import { Book } from 'src/app/models/book';
import { BookService } from 'src/app/service/book.service';
import { CurrentShoppingCartService } from 'src/app/service/current-shopping-cart.service';
import { NotificationService } from 'src/app/service/notification.service';
import { ShoppingCartComponent } from '../shopping-cart/shopping-cart.component';

@Component({
  selector: 'main',
  templateUrl: 'main.component.html',
  styleUrls: ['main.component.css']
})

export class MainComponent implements OnInit {
  elemLength: number = 0;
  currentPageIndex: number = 0;
  pageSize = 15;
  books: Book[];
  
  constructor(
    private bookService: BookService,
    private shoppingCartService: CurrentShoppingCartService,
    private notificationService: NotificationService
  ) {
    
  }

  ngOnInit() {
    this.fetchBooks();
  }

  pageChanged(page) {
    this.pageSize = page.pageSize;
    this.currentPageIndex = page.pageIndex;
    this.fetchBooks();
  }

  private fetchBooks() {
    this.bookService.get(this.currentPageIndex + 1, this.pageSize)
    .subscribe(paginatedBooks => {
      this.elemLength = paginatedBooks.totalElementCount;
      this.books = paginatedBooks.data;
    });
  }

  addToShoppingCart(book) {
    this.shoppingCartService.addToShoppingCart(book, 1)
      .subscribe(resp => {
        this.notificationService.launchSuccess('Elemento agregado a tu carrito');
      }, error => {
        this.notificationService.launchError('Hubo error al agregar este elemento al carrito');
      });
  }
}