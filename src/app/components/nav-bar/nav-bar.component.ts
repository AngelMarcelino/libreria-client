import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'nav-bar',
  templateUrl: 'nav-bar.component.html',
  styleUrls: ['nav-bar.component.css']
})

export class NavBarComponent implements OnInit {
  isAdmin = true;
  constructor(private router: Router) {

  }

  ngOnInit() { }

  goToShoppingCart() {
    this.router.navigate(['shopping-cart'])
  }
}