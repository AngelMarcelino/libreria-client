import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';
import { OrderService } from 'src/app/service/order.service';
import { Order } from '../../models/order';

@Component({
  selector: 'orders',
  templateUrl: 'orders.component.html',
  styleUrls: ['orders.component.css']
})

export class OrdersComponent implements OnInit {
  displayColumns = ['user', 'total', 'details'];
  orders: Order[];
  constructor(
    private ordersService: OrderService,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService
  ) { 
  }
  filterMine = true;
  ngOnInit() {
    this.activatedRoute.data.subscribe(data =>{
      this.filterMine = data.filterMine;
      this.getOrders();
    });
  }

  getOrders() {
    this.ordersService.get()
      .subscribe(orders => {
        this.orders = this.filterMine ? orders.filter(order => order.userId == this.authService.sessionUser.id) : orders;
      });
  }

  getOrderTotal(order: Order){
    return order.books.reduce((accum, curr) => accum + curr.orderBook.price * curr.orderBook.quantity, 0);
  }

  getDetails(order: Order) {
    return order.books.map(b => b.title + ' x ' + b.orderBook.quantity).join(',');
  }
}