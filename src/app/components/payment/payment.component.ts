import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Book } from 'src/app/models/book';
import { AuthService } from 'src/app/service/auth.service';
import { BookService } from 'src/app/service/book.service';
import { CurrentShoppingCartService } from 'src/app/service/current-shopping-cart.service';
import { NotificationService } from 'src/app/service/notification.service';
import { PaymentService } from 'src/app/service/payment.service';
import { calculateTotalAmount } from '../../utils/calculate-total-amount';

const shipmentMethodCostMap = {
  "DHL": 200,
  "UPS": 300,
  "FEDEX": 250,
  "ESTAFETA": 150
};

const shipmentSpeed = {
  "Fast": 2,
  "Normal": 1.5,
  "Slow": 1
}

const shipmentSpeedTranslation = {
  "Fast": 'Rápido',
  "Normal": 'Normal',
  "Slow": 'Lento'
};

@Component({
  selector: 'payment',
  templateUrl: 'payment.component.html'
})

export class PaymentComponent implements OnInit {
  paymentForm: FormGroup;
  shipmentCost: number;
  constructor(
    private formBuilder: FormBuilder,
    private shoppingCartService: CurrentShoppingCartService,
    private paymentService: PaymentService,
    private router: Router,
    private notificationService: NotificationService
  ) {
    this.paymentForm = formBuilder.group({
      shippingMehtod: ['UPS', Validators.required],
      shippingSpeed: ['Normal', Validators.required]
    });

    this.paymentForm.valueChanges.subscribe(
      value => {
        this.shipmentCost = shipmentMethodCostMap[value.shippingMehtod] * shipmentSpeed[value.shippingSpeed];
      }
    );

    this.paymentForm.get('shippingMehtod').setValue('UPS');
    this.paymentForm.get('shippingSpeed').setValue('Normal');

    
  }

  toPayElements: Book[] = [];

  ngOnInit() {
    this.shoppingCartService.get()
      .subscribe(elements => {
        this.toPayElements = elements.books;
      });
  }

  get total() {
    return calculateTotalAmount(this.toPayElements);
  }

  pay() {
    const speed = this.paymentForm.get('shippingSpeed').value;
    const shipment = this.paymentForm.get('shippingMehtod').value;
    const shipmentCost = shipmentMethodCostMap[shipment];
    const supportedInstruments = [
      {
        supportedMethods: "basic-card",
      },
    ];
    
    const details = {
      total: { label: "Compra", amount: { currency: "MXN", value: this.total.toString() } },
      displayItems: 
      this.toPayElements.map(e => ({
        label: e.title + ' x ' + e.shoppingCartBook.quantity,
        amount: {currency: 'MXN', value: (e.price * e.shoppingCartBook.quantity).toString()}
      }))
      ,shippingOptions: [
        {
          id: speed,
          label: shipmentSpeedTranslation[speed],
          amount: { currency: "MXN", value: (shipmentCost * shipmentSpeed[speed]).toString() },
          selected: true,
        },
      ],
    };
    
    const options = { requestShipping: true };
    const self = this;
    async function doPaymentRequest() {
      const request = new PaymentRequest(supportedInstruments, details, options);
      // Add event listeners here.
      // Call show() to trigger the browser's payment flow.
      const response = await request.show();
      // Process payment.
      const json = response.toJSON();
      console.log(json);
      json.shippingCompany = shipment;
      json.shippingMethod = speed;
      self.paymentService.pay(json)
        .subscribe(async () => {
          await response.complete("success");
          self.notificationService.launchSuccess('Gracias por su compra');
          self.shoppingCartService.put([])
            .subscribe(() => {
              self.router.navigate(['/']);
            });
        }, async (err) => {
          await response.complete("fail");
        });
    }
    doPaymentRequest();
  }

}