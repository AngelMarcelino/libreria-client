import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Book } from 'src/app/models/book';
import { CurrentShoppingCartService } from 'src/app/service/current-shopping-cart.service';
import { NotificationService } from 'src/app/service/notification.service';
import { calculateTotalAmount } from '../../utils/calculate-total-amount';

@Component({
  selector: 'shopping-cart',
  templateUrl: 'shopping-cart.component.html',
  styleUrls: ['shopping-cart.component.css']
})

export class ShoppingCartComponent implements OnInit {
  @ViewChild(MatTable) matTable: MatTable<Book>;
  constructor(
    private currentShoppingCartService: CurrentShoppingCartService,
    private notificationService: NotificationService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.currentShoppingCartService.get()
      .subscribe(result => {
        this.dataSource = result.books;
      });

  }

  dataSource: Book[] = [];
  displayedColumns: string[] = ['book', 'quantity', 'price'];

  increaseQuantity(book: Book) {
    book.shoppingCartBook.quantity ++;
    this.saveState();
  }

  decreaseQuantity(book: Book) {
    if (book.shoppingCartBook.quantity == 0) return ;
    if (book.shoppingCartBook.quantity > 1) {
      book.shoppingCartBook.quantity --;
    } else {
      this.notificationService.launchConfirm('Deseas eliminar este producto del carrito?')
        .pipe(
          filter(ok => ok)
        )
        .subscribe(() => {
          this.removeFromCart(book);
        })
    }
    this.saveState();
  }

  removeFromCart(book) {
    const index = this.dataSource.indexOf(book);
    this.dataSource.splice(index, 1);
    this.saveState();
    this.matTable.renderRows();
  }

  get total() {
    return calculateTotalAmount(this.dataSource);
  }

  private saveState() {
    this.currentShoppingCartService.put(this.dataSource.map(book => ({
      bookId: book.id,
      quantity: book.shoppingCartBook.quantity,
      shoppingCartId: book.shoppingCartBook.shoppingCartId
    }))).subscribe();
  }

  goPay() {
    this.router.navigate(['payment']);
  }
}
