import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticatedComponent } from './components/authenticated/authenticated.component';
import { LoginComponent } from './components/login/login.component';
import { MainComponent } from './components/main/main.component';
import { OrdersComponent } from './components/orders/orders.component';
import { PaymentComponent } from './components/payment/payment.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';


const routes: Routes = [{
  path: 'login',
  component: LoginComponent
},{
  path: '',
  component: AuthenticatedComponent,
  children: [
    {
      path: 'main',
      component: MainComponent
    },
    {
      path: 'shopping-cart',
      component: ShoppingCartComponent
    },
    {
      path: 'payment',
      component: PaymentComponent
    },
    {
      path: 'my-orders',
      component: OrdersComponent,
      data: {
        filterMine: true
      }
    },
    {
      path: 'orders',
      component: OrdersComponent,
      data: {
        filterMine: false
      }
    }
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
