import { Book } from '../models/book';
export function calculateTotalAmount(books: Book[]) {
  return books.reduce((accum, current) => accum + current.price * current.shoppingCartBook.quantity, 0)
}
