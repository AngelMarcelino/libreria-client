import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({providedIn: 'root'})
export class NotificationService {
  constructor() { }

  launchSuccess(message){
    alert(message);
  }

  launchError(message) {
    alert(message);
  }

  launchConfirm(message) {
    return of(confirm(message));
  }
  
}