import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Book } from '../models/book';
import { PaginatedData } from '../models/paginated-data';

@Injectable({providedIn: 'root'})
export class BookService {
  constructor(private httpClient: HttpClient) { }

  get(pageNumber: number, pageSize: number): Observable<PaginatedData<Book>> {
    const elementOffset = (pageNumber - 1) * pageSize;
    return this.httpClient.get<Book[]>('/books')
      .pipe(
        map(allBooks => ({data: allBooks.slice(elementOffset, elementOffset + pageSize), totalElementCount: allBooks.length}))
      );
  }
  
}