import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, switchMap, tap } from 'rxjs/operators';
import { Book } from '../models/book';
import { AuthService } from './auth.service';
import { ShoppingCart } from '../models/shopping-cart';
import { ShoppingCartEntry } from '../models/shopping-cart-entry';
import { typeWithParameters } from '@angular/compiler/src/render3/util';

@Injectable({providedIn: 'root'})
export class CurrentShoppingCartService {
  constructor(private httpClient: HttpClient, private authService: AuthService) { }

  get() {
    const currentUser = this.authService.sessionUser;
    return this.httpClient.get<ShoppingCart>(`/users/${currentUser.id}/shoppingCart`);
  }

  put(shoppingCartEntries: ShoppingCartEntry[]) {
    const currentUser = this.authService.sessionUser;
    return this.httpClient.put(`/users/${currentUser.id}/shoppingCart`, shoppingCartEntries);
  }

  addToShoppingCart(book: Book, quantity: number) {
    return this.get()
      .pipe(
        tap(shoppingCart => {
          console.log(shoppingCart);
        }),
        map(shoppingCart => {
          return this.addBook(shoppingCart, book);
        }),
        switchMap(currentShoppingCartBooks => {
          return this.put(currentShoppingCartBooks);
        })
      );
  }

  addBook(shoppingCart: ShoppingCart, book: Book) {
    let insertedBook = false;
    const shoppingCartItems: ShoppingCartEntry[] = shoppingCart.books.map(bookInCart => {
      let quantity = bookInCart.shoppingCartBook.quantity;
      if (book.id == bookInCart.id) {
        quantity += 1;
        insertedBook = true;
      }
      return {
        bookId: bookInCart.id,
        quantity: quantity,
        shoppingCartId: shoppingCart.id
      } as ShoppingCartEntry
    });

    if (!insertedBook) {
      return [...shoppingCartItems, {
        bookId: book.id,
        quantity: 1,
        shoppingCartId: shoppingCart.id
      }];
    } else {
      return shoppingCartItems;
    }
  }

}
