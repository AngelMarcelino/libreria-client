import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable({providedIn: 'root'})
export class PaymentService {
  constructor(private httpClient: HttpClient, private authService: AuthService) {

  }

  pay(payObject: any) {
    return this.httpClient.post('/shoppingCart/pay/'+ this.authService.sessionUser.id, payObject);
  }
  
}