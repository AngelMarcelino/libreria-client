import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Order } from '../models/order';

@Injectable({providedIn: 'root'})
export class OrderService {
  constructor(private httpClient: HttpClient) { }

  get() {
    return this.httpClient.get<Order[]>('/orders');
  }
  
}