import { Injectable } from '@angular/core';
import { fromEventPattern } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LoginModel } from '../models/login-model';
import { RegisterModel } from '../models/register-model';
import { tap } from 'rxjs/operators';
import { SessionUser } from '../models/session-user';

export const TOKEN_STORE_KEY = 'auth_token';

@Injectable({providedIn: 'root'})
export class AuthService {
  private _sessionUser: SessionUser;
  get sessionUser(): SessionUser {
    return this._sessionUser;
  }

  constructor(
    private httpClient: HttpClient
  ) {
    this.checkToken();
  }

  private checkToken() {
    const token = this.getToken();
    const tokenObj = this.parseJwt(token);
    console.log(tokenObj);
    this._sessionUser = {
      id: tokenObj.sub
    };
  }

  private parseJwt (token) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
  }

  login(loginModel: LoginModel) {
    return this.httpClient.post('/account/token', loginModel, {responseType: 'text'})
      .pipe(
        tap(token => {
          console.log(token);
          this.saveToken(token);
        })
      );
  }

  private saveToken(token){ 
    localStorage.setItem(TOKEN_STORE_KEY, token);
  }

  register(registerModel: RegisterModel) {
    return this.httpClient.post('/account/register', registerModel);
  }

  getToken() {
    return localStorage.getItem(TOKEN_STORE_KEY);
  }
  
}
