import { Book } from './book';

export interface ShoppingCart {
  id: number;
  userId: number;
  books: Book[];
}