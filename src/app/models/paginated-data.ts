export interface PaginatedData<T> {
  data: T[];
  totalElementCount: number;}