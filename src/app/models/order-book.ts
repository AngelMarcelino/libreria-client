export interface OrderBook {
  price: number;
  quantity: number;
}