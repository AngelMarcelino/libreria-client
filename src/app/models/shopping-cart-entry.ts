export interface ShoppingCartEntry {
  shoppingCartId: number;
  bookId: number;
  quantity: number;
  createdAt?: string | Date;
  updatedAt?: string | Date;
}
