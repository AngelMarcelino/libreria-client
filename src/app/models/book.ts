import { ShoppingCartEntry } from './shopping-cart-entry';
import { OrderBook } from './order-book';

export interface Book {
  id: number;
  title: string;
  description: string;
  isbn: string;
  author: string;
  price: number;
  shoppingCartBook?: ShoppingCartEntry;
  orderBook: OrderBook;
}
