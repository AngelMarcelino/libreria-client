
import { Book } from './book';
import { User } from './user';
export interface Order {
  id: number;
  userId: number;
  user: User;
  books: Book[];
  shippingMethod: string;
  shippingCompany: string;
  orderData: string;
}
