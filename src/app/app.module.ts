import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { LoginComponent } from './components/login/login.component';
import { AuthService } from './service/auth.service';
import { AuthenticatedComponent } from './components/authenticated/authenticated.component';
import {NotificationService} from './service/notification.service';
import { MainComponent } from './components/main/main.component';
import {MatButtonModule} from '@angular/material/button';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APIInterceptor } from './service/http-global-interceptor';
import { AuthInterceptor } from './service/http-auth-interceptor';
import { MatPaginatorModule } from '@angular/material/paginator';
import { BookService } from './service/book.service';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import { CurrentShoppingCartService } from './service/current-shopping-cart.service';
import { MatTableModule } from '@angular/material/table';
import {PaymentComponent} from './components/payment/payment.component';
import {PaymentService} from './service/payment.service';
import {OrdersComponent} from './components/orders/orders.component';
import { OrderService} from './service/order.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AuthenticatedComponent,
    MainComponent,
    NavBarComponent,
    ShoppingCartComponent,
    PaymentComponent,
    OrdersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatPaginatorModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTableModule
  ],
  providers: [
    AuthService,
    NotificationService,
    BookService,
    ShoppingCartComponent,
    CurrentShoppingCartService,
    OrderService,
    PaymentService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: APIInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
